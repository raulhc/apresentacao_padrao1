# Objetivo

Essa paletra contém informações da fundação do Raul, o que ja fizemos e quais os projetos que temos hoje.

# Onde já realizamos

## Palestra do FISL 15

Ela aconteceu no dia 07 de maio de 2014 às 16:00

Segue abaixo o vídeo para assistir a gravação:

(hemingway.softwarelivre.org/fisl15/high/41d/sala41d-high-201405071602.ogv)

## EREDS Nordeste

Data: 09/08/2014 às 16:20

https://eredsne.wordpress.com/programacao/

## Software Freedom Day Salvador 2014

Data: 27/09/2014 às 15:10

http://softwarelivre.org/sfdssa/programacao-2014

## XV ERBASE (Escola Regional de Computação Bahia – Alagoas – Sergipe)

Data: 15/04/2015 às 13:30

http://erbase2015.ifba.edu.br/programacao-geral.php

# Licença

Attribution-NonCommercial 4.0 International 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-nc/4.0/80x15.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a>
